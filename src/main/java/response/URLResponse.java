package response;

public class URLResponse {
    private String shortUrl;

    private boolean success;

    public URLResponse(String shortUrl, boolean success) {
        this.shortUrl = shortUrl;
        this.success = success;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
