package request;

public class URLRequest {

    private String longUrl;
    private String seoKeyword;

    public URLRequest(String longUrl, String seoKeyword) {
        this.longUrl = longUrl;
        this.seoKeyword = seoKeyword;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    public String getSeoKeyword() {
        return seoKeyword;
    }

    public void setSeoKeyword(String seoKeyword) {
        this.seoKeyword = seoKeyword;
    }
}
