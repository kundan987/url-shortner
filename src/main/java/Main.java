public class Main {
    public static void main(String[] args) {
        System.out.println("Hello World");
    }

    public void display(){
        System.out.println("Hello World");
    }

    /**
     * Shorten URL "SEO"

     Given as input a URL and a SEO keyword with a max length of 20 characters, chosen by the user, generate a SEO URL.

     Example:

     Input:
     URL: http://looooong.com/somepath
     SEO keyword: MY-NEW-WS
     Output:
     URL: http://short.com/MY-NEW-WS

     Input:
     URL: http://looooong.com/somepath
     SEO keyword: POTATO
     Output:
     URL: http://short.com/POTATO
     */
}
