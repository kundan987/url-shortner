package service;

import request.URLRequest;
import response.URLResponse;

import java.util.HashMap;
import java.util.Random;

public class URLShortnerServiceImpl implements URLShortnerService{

    HashMap<String, String> shortToLongUrlMap = new HashMap<>();

    private static final String BASE_URL = "http://short.com/";

    @Override
    public URLResponse createShortUrl(URLRequest request) {
        //shortToLongUrlMap.put(request.getSeoKeyword(), request.getLongUrl());
        String shortUrl = BASE_URL;
        if(validateRequest(request)){
            if(request.getSeoKeyword() == null || request.getSeoKeyword().isEmpty()){
                shortUrl +=generateShortUrl();
            } else {
                shortUrl +=request.getSeoKeyword();
            }
            return new URLResponse(shortUrl, true);
        }
        else
            return new URLResponse(null, false);
    }

    private String generateShortUrl() {
        char[] alphabet = {'a', 'b', 'c', '1', '2', '3'}; //26+10 = 0-36

        StringBuilder sb = new StringBuilder();
        for(int i=0; i<4; i++){
            Random random = new Random();
            int index = random.nextInt(6);
            sb.append(alphabet[index]);
        }
        return sb.toString();
    }


    private boolean validateRequest(URLRequest request) {
        if(request == null ||  request.getLongUrl() == null || request.getLongUrl().isEmpty())
            return false;
        return true;
    }

    @Override
    public URLResponse getLongUrl(String shortUrl) {
        return null;
    }
}
