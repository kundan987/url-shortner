package service;

import request.URLRequest;
import response.URLResponse;

public interface URLShortnerService {

    URLResponse createShortUrl(URLRequest request);

    URLResponse getLongUrl(String shortUrl);
}
