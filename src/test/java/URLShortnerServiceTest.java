import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import request.URLRequest;
import response.URLResponse;
import service.URLShortnerService;
import service.URLShortnerServiceImpl;

public class URLShortnerServiceTest {

    URLShortnerService service;

    @Before
    public void setup(){
        service = new URLShortnerServiceImpl();
    }

    @Test
    public void createShortUrlTest(){
        URLResponse response = service.createShortUrl(new URLRequest("http://looooong.com/somepath", "MY-NEW-WS"));
        Assert.assertEquals("http://short.com/MY-NEW-WS", response.getShortUrl());
    }

    @Test
    public void requestNullTest(){
        URLResponse response = service.createShortUrl(null);
        Assert.assertFalse(response.isSuccess());
    }

    @Test
    public void requestKeywordNull(){
        URLResponse response = service.createShortUrl(new URLRequest("http://looooong.com/somepath", null));
        Assert.assertFalse(response.isSuccess());
    }

    @Test
    public void requestKeywordEmpty(){
        URLResponse response = service.createShortUrl(new URLRequest("http://looooong.com/somepath", ""));
        Assert.assertFalse(response.isSuccess());
    }

    @Test
    public void requestLongUrlNull(){
        URLResponse response = service.createShortUrl(new URLRequest(null, "Hello"));
        Assert.assertFalse(response.isSuccess());
    }

    @Test
    public void requestLongUrlEmpty(){
        URLResponse response = service.createShortUrl(new URLRequest("", "Hello"));
        Assert.assertFalse(response.isSuccess());
    }

    @Test
    public void requestKeywordExceedLength(){
        URLResponse response = service.createShortUrl(new URLRequest("http://looooong.com/somepath", "HELLO-WORLD-HELLO-WORLD"));
        Assert.assertFalse(response.isSuccess());
    }

    @Test
    public void createGenericShortUrlTest(){
        URLResponse response = service.createShortUrl(new URLRequest("http://looooong.com/somepath", null));
        System.out.println(response.getShortUrl());

    }

}
